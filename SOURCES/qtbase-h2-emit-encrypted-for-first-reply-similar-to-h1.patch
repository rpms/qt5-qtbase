From 95064c35826793c5d6a4edff9fa08ad308b047bb Mon Sep 17 00:00:00 2001
From: Timur Pocheptsov <timur.pocheptsov@qt.io>
Date: Tue, 20 Jul 2021 08:16:28 +0200
Subject: [PATCH] H2: emit encrypted for at least the first reply, similar to
 H1
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit

Fixes: QTBUG-95277
Change-Id: I1fe01503376c0d6278e366d7bd31b412b7cc3a69
Reviewed-by: Mårten Nordheim <marten.nordheim@qt.io>
(cherry picked from commit c23b7886348dc313ccec1a131850a7cce1b429de)
---
 src/network/access/qhttpnetworkconnectionchannel.cpp | 4 ++++
 tests/auto/network/access/http2/tst_http2.cpp        | 9 +++++++++
 2 files changed, 13 insertions(+)

diff --git a/src/network/access/qhttpnetworkconnectionchannel.cpp b/src/network/access/qhttpnetworkconnectionchannel.cpp
index f1db2744..647ef431 100644
--- a/src/network/access/qhttpnetworkconnectionchannel.cpp
+++ b/src/network/access/qhttpnetworkconnectionchannel.cpp
@@ -1284,6 +1284,10 @@ void QHttpNetworkConnectionChannel::_q_encrypted()
         connection->connectionType() == QHttpNetworkConnection::ConnectionTypeHTTP2Direct) {
         // we call setSpdyWasUsed(true) on the replies in the SPDY handler when the request is sent
         if (spdyRequestsToSend.count() > 0) {
+            // Similar to HTTP/1.1 counterpart below:
+            const auto &pairs = spdyRequestsToSend.values(); // (request, reply)
+            const auto &pair = pairs.first();
+            emit pair.second->encrypted();
             // In case our peer has sent us its settings (window size, max concurrent streams etc.)
             // let's give _q_receiveReply a chance to read them first ('invokeMethod', QueuedConnection).
             QMetaObject::invokeMethod(connection, "_q_startNextRequest", Qt::QueuedConnection);
diff --git a/tests/auto/network/access/http2/tst_http2.cpp b/tests/auto/network/access/http2/tst_http2.cpp
index 6702f25b..95a97902 100644
--- a/tests/auto/network/access/http2/tst_http2.cpp
+++ b/tests/auto/network/access/http2/tst_http2.cpp
@@ -267,6 +267,10 @@ void tst_Http2::singleRequest()
     request.setAttribute(h2Attribute, QVariant(true));
 
     auto reply = manager->get(request);
+#if QT_CONFIG(ssl)
+    QSignalSpy encSpy(reply, &QNetworkReply::encrypted);
+#endif // QT_CONFIG(ssl)
+
     connect(reply, &QNetworkReply::finished, this, &tst_Http2::replyFinished);
     // Since we're using self-signed certificates,
     // ignore SSL errors:
@@ -281,6 +285,11 @@ void tst_Http2::singleRequest()
 
     QCOMPARE(reply->error(), QNetworkReply::NoError);
     QVERIFY(reply->isFinished());
+
+#if QT_CONFIG(ssl)
+    if (connectionType == H2Type::h2Alpn || connectionType == H2Type::h2Direct)
+        QCOMPARE(encSpy.count(), 1);
+#endif // QT_CONFIG(ssl)
 }
 
 void tst_Http2::multipleRequests()
